package main

import (
	"crypto/sha512"
	"fmt"
	"github.com/ProtonMail/gopenpgp/v2/crypto"
	"github.com/jessevdk/go-flags"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
)

const pubkey = `
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBF3YIJQBEADbuy4oFx43M3WXfD0sl+Qi/RFk/eiN4naOkSGn6Cn1MTjB6TuM
qD8PcYqxb/ABE9mEqtvLFi8M8ZJw72abpOHjqYsYwzzwlgSA+61IphYM2EALdmdc
rauUuYiXxY2qTF9P2Yx/gJgDw3CRojchlE5aULs6khLOnZCoFHQdgwEsrBlkPTR4
FM2ql30N58SHZAuaIkpeNfetbMsQ42gbqz++1B+yhN4WsMR+TosCid1JL86bheSS
PPZW3uNP58sCjFVIwkeUMWuMJjS2iFPX0o8gv6g07pdP22JvzOxXY9ZsPPNqLQIW
ldpnww1yED4j4NKKJeqkBFDm51zpXpZJKbBO9hmDzDrZyVpEEJwFFQ0hYucqKgBF
l5MY1JbstuUMTb86S0TD4uCDNhyQjVWIqT8VU+pt4e9f1HiXJAh55NwEJIbhtT4o
YEhYpsp51RAgVR72FeOg/pDpm1/OdE9M5tKAanlsIPKvbPkDHn2+wYVtwv2UP/LS
3VDc8sYCPXhX93TcudIK36sSNRK0p0YDW0OSwViJ5cJPYatYVJc4ac+jHvkrENGl
6j1kNhKpDpNDLMlixKp+zs5swlPNXnU6RxvTr7AgKH7bn6NpYDjKatSCXdRPVz46
dWWVFRP8U36Lv74qvvg7i8XJ9jajM8kKhLGCv08EFqXPbGzAwlmd+Jx46wARAQAB
tExSZWFsdGltZSBSb2JvdGljcyBMaW51eCBQYWNrYWdlcyBLZXltYXN0ZXIgPGxp
bnV4LXBhY2thZ2VzLWtleW1hc3RlckBydHIuYWk+iQJTBBMBCgA+FiEE0jK/HFTg
9LHx0RWSDC5RpqNJC5EFAl3YIJQCGwMFCRLMAwAFCwkIBwIGFQoJCAsCBBYCAwEC
HgECF4AACgkQDC5RpqNJC5EEDw/3eTMxu+Y5Lec4jrjV52Fs4aefr1c27KZDJNDz
P25Jkun7hWzfm9V+A2oJx5LWhpBh1YiouAAlSNFdoBfP5Ek9bhv5u6UGimrweIId
J2C40s+4ZEMF5GaV5HrEr5YAE/eARvBZtJQZ11nzN6+YtnaCRJ0ZTTTzSqNX2Sme
Cw6iRa5Omd+ufdrc0PLvjgFJPt1bCqTf1ps/74V0VFmRh4Gqm+CcHYZkllPI+37p
RGEqeYR5zIZqCI+Dyl7SNKv18WguuWWmiQrimEYZd5HmYwCvOXu+J63AxS9e/J+5
yf90BpRXtfL1tJOWl7Gsry6/AhzXn9lvlN8AbVKmyZYSRl+ZYyi12MRpuYiDwqDG
gYZMskhFHD51+W1uOoI5Gf6GsHpr+IE/nuRFA8K8neLDHhUr08o9Nvi8C0S/b+zM
vMUn+KZpIQfNuvUDHsccz2UiqZtS6iGKjHiV+b5UGKxY7/k07dCZtUlUMkO3v3Jl
5vrCMHH9gc44q83090kEYWA8jNHqlb6mTNcCp44v3c+iVqVLO87HIHlX+DsCyaW4
SOhUw3OcazrJdY6JaO2YvRUFxSAfpF51F378ZG+8Fv5Bb3czdueTc9K8if+4KY9b
ySo0cewS4M6mJh0RQRgmZ4IiCyjTX65ZlRT6ha6GTE/SSfNEoXWwByvNJaSGqICk
TUrccLkCDQRd2CCUARAA8RLE3c1HmcZnOOAzFFKEOOUiBHT0rYboysT3bRd2DrGj
5bxbkxEm6HWZPJnBboGTKaDGvlgOwCHJo+K3HxKEzfzutRAaXs630/wNjggQdyNl
9lKcuudjgSRMElAJ19qUX/zvzOcmL/TfI5KeW1a1IacAFMUSzhWDdC9jKiaGcJVf
zrlEwFs2iUpmRVMPas+hjKe9LhLBSRz7AkrPIvdIhL9KUQ9AddjjVVBpAjMBkrbY
F6pkbb3z3KiaOL9nii8TDH49hUsr2hLcgQWApeGnMSr/PP3mR5OaC2elAK7sv0ST
BQZblbSpgxDevyJBXQdAhOByfu9Qyao680WPc7/wu1KTkUbvQj5+wLpvGC9Ry/0l
S9VT8OSe7/c+J8yEtSiDCyleECLlmL9kdG0Vm7quQl4KyD/KkZ3skCnv9b3Xa9bp
LN5K0Smb1Fx2+Xk3y9xJxotWZk7piIVAebvjY++dROeycz9BtTrgUrrLZFyKe6zz
ZZHHgiX4k445ioJxSolU4RYBn9gvIpGFntgKWwJakXdSwZM+Sb8j1AujVJDGE628
ijqm6uDzbYAU5pJjoH9z4I6/ssWzJKJ/fma441xWOTwNVh+uqhqs0LhARSFIC/Jv
UgZY+WK4ZAru/Iga/XdrXkveEgorbjB6vg0v5XO7JHTUAr54+acmYlhyYAoUAnMA
EQEAAYkCPAQYAQoAJhYhBNIyvxxU4PSx8dEVkgwuUaajSQuRBQJd2CCUAhsMBQkS
zAMAAAoJEAwuUaajSQuRIUEQAJGXTAukrp9rGv7oJC3cdslSrqKen6+TBk4556sa
MsMuHAlnDrr4ujG2sTEZyBp4ciD5QJE2wjO5YD19YKOGCy+RXcE6cJNh9qRlZ89/
fV86GnStkGSL5WtyJxkJoHJAPZq/oNIwewPmPoYfzk8zGpWhZVmDLwXIGvw0zhII
MC7+lv4bjcfLGwLKlcR2ZJ64m06jaUfxJDsdjTKDqxq1qZVVpcfSslDuSvKkaHMG
YM+bR8x1f5vO5N2UxDlImavVu/7aLiJwg6rDjYJBkRwqKYOacKtmoiGJH2mMKpHH
E+1t8AdwW0W5TmKNydZAexHKyYCb5DHISGZaJSw85jOx+ezQEXDPaqlhKw2m3Jnc
vzr0KsB6Sb3p4mu/34CseUz6Gc6W3u5qs1ORYTnoWfTebVuYDhDYFO39IgpWobxE
99V5qMMtMAUzGsGQxxqLu0uOH7a8OhIWWzGPXihH5Vdb7dSWMdQi+9gDga8wWmgV
BYzbTFIz6VtG9QDutqgFgkKc9FpKZWFqJRmWL4Yccnzd+soi7HFLm9JU2eImrx/k
GrxLt5AjBV99A2uYQl3LOeWDGFlKKHVqZPS/PfQXQVC+fl41gh5/G+wUwkFyHg/l
j/93yobV94zgNVIH7wihZ1IKjUctrwfwhlRPepD0dVT65gjsyC2GJ5kC2farSbA9
nZWf
=YX62
-----END PGP PUBLIC KEY BLOCK-----
`

var (
	selfpackage string = "unknown"
	selfversion string = "unknown"
	this        string
	here        string
	whatami     string
	opts        struct {
		Noop       bool `short:"n" long:"dry-run" description:"verify without executing"`
		Verbose    bool `short:"v" long:"verbose" description:"show verbose debug information"`
		Version    bool `short:"V" long:"version" description:"print version and exit"`
		Positional struct {
			Executable string `positional-arg-name:"EXECUTABLE"`
		} `positional-args:"yes"`
	}
	parser *flags.Parser
)

func _info(format string, v ...interface{}) {
	if opts.Verbose {
		log.Printf("INFO: "+format, v...)
	}
}

func _error(format string, v ...interface{}) {
	log.Printf("ERROR: "+format, v...)
}

func _die(format string, v ...interface{}) {
	_error(format, v...)
	if nil != parser {
		parser.WriteHelp(os.Stderr)
	}
	os.Exit(1)
}

func _getenv_nonempty(key string) string {
	res := os.Getenv(key)
	if "" == res {
		_die("undefined/empty environment variable: %v", key)
	}
	return res
}

func _getenv_uint(key string) uint64 {
	res, err := strconv.ParseUint(_getenv_nonempty(key), 10, 64)
	if nil != err {
		_die("%v", err)
	}
	return res
}

func _read_nonempty(path string) []byte {
	res, err := ioutil.ReadFile(path)
	if nil != err {
		_die("failed to read %v: %v", path, err)
	}
	if 0 == len(res) {
		_die("empty file: %v", path)
	}
	return res
}

func main() {

	this, err := os.Executable()
	if nil != err {
		_die("os.Executable(): %v", err)
	}
	whatami := filepath.Base(this)
	log.SetFlags(0)
	log.SetPrefix(fmt.Sprintf("%v[%v]: ", whatami, os.Getpid()))
	here := filepath.Dir(this)

	parser = flags.NewParser(&opts, flags.Default)
	args, err := parser.ParseArgs(os.Args[1:])
	if opts.Version {
		fmt.Printf("%v (%v) %v\n", whatami, selfpackage, selfversion)
		os.Exit(0)
	}
	if nil != err {
		e, ok := err.(*flags.Error)
		if ok && e.Type == flags.ErrHelp {
			os.Exit(0)
		}
		parser.WriteHelp(os.Stderr)
		os.Exit(1)
	}
	if 0 != len(args) {
		_die("too many arguments")
	}
	if "" == opts.Positional.Executable {
		_die("missing EXECUTABLE")
	}

	_info("this: %v", this)
	_info("here: %v", here)
	_info("whatami: %v", whatami)
	_info("opts.Noop: %v", opts.Noop)
	_info("opts.Verbose: %v", opts.Verbose)
	_info("opts.Positional.Executable: %v", opts.Positional.Executable)

	//////////////////////////
	// vet sudo environment //
	//////////////////////////

	// effective uid
	if 0 != os.Geteuid() {
		_die("non-zero euid: %v", os.Geteuid())
	}
	_info("good euid: %v", os.Geteuid())

	// effective gid
	if 0 != os.Getegid() {
		_die("non-zero egid: %v", os.Getegid())
	}
	_info("good egid: %v", os.Getegid())

	// SUDO_USER
	sudo_user := _getenv_nonempty("SUDO_USER")
	if "root" == sudo_user {
		_die("bad SUDO_USER: %v", sudo_user)
	}
	_info("good SUDO_USER: %v", sudo_user)

	// SUDO_UID
	sudo_uid := _getenv_uint("SUDO_UID")
	if 0 == sudo_uid {
		_die("bad SUDO_UID: %v", sudo_uid)
	}
	_info("good SUDO_UID: %v", sudo_uid)

	// SUDO_GID
	sudo_gid := _getenv_uint("SUDO_GID")
	if 0 == sudo_gid {
		_die("bad SUDO_GID: %v", sudo_gid)
	}
	_info("good SUDO_GID: %v", sudo_gid)

	///////////////////////////////////////
	// find executable and signed digest //
	///////////////////////////////////////
	executable, err := filepath.Abs(opts.Positional.Executable)
	if nil != err {
		_die("%v", err)
	}
	if fileinfo, err := os.Stat(executable); nil != err {
		_die("%v", err)
	} else {
		if !fileinfo.Mode().IsRegular() {
			_die("not regular file: %v", executable)
		}
		if 0111 != fileinfo.Mode()&0111 {
			_die("not executable by all: %v %v", fileinfo.Mode(), executable)
		}
	}
	_info("found EXECUTABLE: %v", executable)
	check_asc := executable + ".check.asc"
	if fileinfo, err := os.Stat(check_asc); nil != err {
		_die("%v", err)
	} else {
		if !fileinfo.Mode().IsRegular() {
			_die("not regular file: %v", check_asc)
		}
		if 0444 != fileinfo.Mode()&0444 {
			_die("not readable by all: %v %v", fileinfo.Mode(), check_asc)
		}
	}
	_info("found signed digest: %v", check_asc)

	////////////////////////
	// read signed digest //
	////////////////////////
	clearTextMessage, err := crypto.NewClearTextMessageFromArmored(
		string(_read_nonempty(check_asc)))
	if nil != err {
		_die("failed to parse %v: %v", check_asc, err)
	}
	_info("read signed digest: %v", check_asc)

	//////////////////////
	// verify signature //
	//////////////////////
	publicKey, err := crypto.NewKeyFromArmored(pubkey)
	if nil != err {
		_die("key initialization failure: %v", err)
	}
	publicKeyRing, err := crypto.NewKeyRing(publicKey)
	if nil != err {
		_die("keyring initialization failure: %v", err)
	}
	err = publicKeyRing.VerifyDetached(
		crypto.NewPlainMessage(clearTextMessage.GetBinary()),
		crypto.NewPGPSignature(clearTextMessage.GetBinarySignature()),
		crypto.GetUnixTime())
	if nil != err {
		_die("%v", err)
	}
	_info("good signature")

	///////////////////
	// verify digest //
	///////////////////
	computed_digest := fmt.Sprintf(
		"SHA512 (%s) = %x",
		filepath.Base(executable),
		sha512.Sum512(_read_nonempty(executable)),
	)
	_info("computed digest: %v", computed_digest)
	reported_digest := strings.TrimSpace(clearTextMessage.GetString())
	_info("reported digest: %v", reported_digest)
	if reported_digest != computed_digest {
		_die(`
  digest verification failed: %v
    %v:
      %v
    should be:
      %v

`,
			executable, check_asc, reported_digest, computed_digest)
	}
	_info("verifed digest")

	///////////////////////
	// invoke executable //
	///////////////////////
	cmd := exec.Command(executable)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if opts.Noop {
		err = nil
		_info("NOOP: skipping execution")
	} else {
		err = cmd.Run()
	}
	if nil != err {
		_die("FAILURE: %v: %v", cmd.String(), err)
	}
	_info("SUCCESS: %v", cmd.String())
	_info("done")
}
